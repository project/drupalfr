<?php

/**
 * Return an array of the modules to be enabled when this profile is installed.
 *
 * @return
 *  An array of modules to be enabled.
 */
function fr_profile_modules() {
  return array('block', 'color', 'comment', 'filter', 'help', 'menu', 'node', 'system', 'taxonomy', 'user', 'watchdog', 'locale');
}

/**
 * Return a description of the profile for the initial installation screen.
 *
 * @return
 *   An array with keys 'name' and 'description' describing this profile.
 */
function fr_profile_details() {
  return array(
    'name' => 'Drupal en Francais',
    'description' => 'Selectionnez ce profil pour installer Drupal basic en francais'
  );
}

/**
 * Perform any final installation tasks for this profile.
 *
 * @return
 *   An optional HTML string to display to the user on the final installation
 *   screen.
 */
function fr_profile_final() {
  // Insert default user-defined node types into the database.
  $types = array(
    array(
      'type' => 'page',
      'name' => st('Page'),
      'module' => 'node',
      'description' => st('If you want to add a static page, like a contact page or an about page, use a page.'),
      'custom' => TRUE,
      'modified' => TRUE,
      'locked' => FALSE,
    ),
    array(
      'type' => 'story',
      'name' => st('Story'),
      'module' => 'node',
      'description' => st('Stories are articles in their simplest form: they have a title, a teaser and a body, but can be extended by other modules. The teaser is part of the body too. Stories may be used as a personal blog or for news articles.'),
      'custom' => TRUE,
      'modified' => TRUE,
      'locked' => FALSE,
    ),
  );

  foreach ($types as $type) {
    $type = (object) _node_type_set_defaults($type);
    node_type_save($type);
  }

  // Default page to not be promoted and have comments disabled.
  variable_set('node_options_page', array('status'));
  variable_set('comment_page', COMMENT_NODE_DISABLED);

  // Don't display date and author information for page nodes by default.
  $theme_settings = variable_get('theme_settings', array());
  $theme_settings['toggle_node_info_page'] = FALSE;
  variable_set('theme_settings', $theme_settings);

  // http://drupal.org/node/67921
  /* Import current install language files
   * TODO This is only a HACK until autolocale module becomes available...
   *
   * copy the small "[isocode]/install.po" into profiles/myprofile/[isocode].po
   * copy the bigger "[isocode]/de.po" into profiles/myprofile/po/[isocode].po
   */
  global $profile, $install_locale;
  static $mode = 'overwrite';

  // Add language, if not yet supported
  $languages = locale_supported_languages(TRUE, TRUE);
  if (!isset($languages['name'][$install_locale])) {
    $isocodes = _locale_get_iso639_list();
    _locale_add_language($install_locale, $isocodes[$install_locale][0], FALSE);
  }

  $filename = './profiles/' . $profile . '/po/fr.po';
  if (file_exists($filename)) {
    $file = (object) array('filepath' => $filename);
    if ($ret = _locale_import_po($file, $install_locale, $mode) == FALSE) {
      $message = t('The translation import of %filename failed.', array('%filename' => $file->filename));
    }
    else {
      // import successfull, enable selected language and make standard
      db_query("UPDATE {locales_meta} SET isdefault = 0 WHERE locale = '%s'", 'en');
      db_query("UPDATE {locales_meta} SET enabled = 1, isdefault = 1 WHERE locale = '%s'", $install_locale);
    }
  }

}
