Instructions d'installations

1. copier le dossier fr dans votre dossier profiles
2. creer un dossier po dans ce dossier profiles/fr
3. telecharger la derniere version de la translation francaise de drupal
	http://drupal.org/project/fr
4. creer les dossier profiles/fr et profiles/fr/po
5. copier fr.po dans le dossier profiles/fr/po
7. copier installer.po dans profiles/fr
8. renommer profiles/fr/installer.po profiles/fr/fr.po

9. visiter http://exemple.com/
